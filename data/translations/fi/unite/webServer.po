# Strings used in Opera Unite applications.
# Copyright (C) 2009 Opera Software ASA
# This file is distributed under the same license as Opera Unite applications.
# Anders Sjögren <anderss@opera.com>, 2009.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2009-09-02 10:22-02:00\n"
"PO-Revision-Date: 2009-11-09 14:37+0100\n"
"Last-Translator: Anne Lilleholt\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"MIME-Version: 1.0\n"

#. Error page title text when a resource is not found
#: templates/fileSharing.html
msgid "Folder or file not found"
msgstr "Kansiota tai tiedostoa ei löytynyt"

#. A table header that describes the access level for a file in the selected folder.
#: templates/fileSharing.html
msgid "Access"
msgstr "Käyttötaso"

#. A table header that describes the name of a file in the selected folder.
#: templates/fileSharing.html
msgid "Name"
msgstr "Nimi"

#. A table header that describes the size of a file in the selected folder.
#: templates/fileSharing.html
msgid "Size"
msgstr "Koko"

#. A table header that describes the time a file last got modified in the selected folder.
#: templates/fileSharing.html
msgid "Time"
msgstr "Aika"

#. A link for a visitor to download a file from the owner's selected folder.
#: templates/fileSharing.html
msgid "Download"
msgstr "Lataa"

#. Singular case
#. From the line below the list of files "2 folders and 8 files"
#: templates/fileSharing.html
msgid "1 folder"
msgstr "1 kansio"

#. Plural case
#. From the line below the list of files "2 folders and 8 files"
#: templates/fileSharing.html
msgid "{counter} folders"
msgstr "{counter} kansiota"

#. From the line below the list of files "2 folders and 8 files"
#: templates/fileSharing.html
msgid "and"
msgstr "ja"

#. Singular case
#. From the line below the list of files "2 folders and 8 files"
#: templates/fileSharing.html
msgid "1 file"
msgstr "1 tiedosto"

#. Plural case
#. From the line below the list of files "2 folders and 8 files"
#: templates/fileSharing.html
msgid "{counter} files"
msgstr "{counter} tiedostoa"

#. Text displayed when there is no index.html file in the owner's Web Server folder.
#: templates/messages.html
msgid "Visitors will see the files in this folder, as there is no index.html file to display."
msgstr "Vierailijat näkevät tämän kansion tiedostot, koska näytettävää index.html-tiedostoa ei ole."

#. Text displayed when there is no index.html file in the owner's Web Server folder.
#: templates/messages.html
msgid "If you want visitors to see an index page, create an index.html file or <A href=\"?create_index=true\">generate a sample file</A>."
msgstr "Jos haluat, että vierailijat näkevät index-sivun, luo index.html-tiedosto tai <A href=\"?create_index=true\">esimerkkitiedosto</A>."

#. Text displayed when there is an index.html file in the owner's Web Server folder.
#: templates/messages.html
msgid "<EM>This folder contains an index.html file.</EM> This is the first page visitors to your Web Server will see: <A href=\"{index}\">{index}</A>"
msgstr "<EM>Tämä kansio sisältää index.html-tiedoston.</EM> Tämä on ensimmäinen sivu, jonka web-palvelimesi vierailijat näkevät: <A href=\"{index}\">{index}</A>"

#. Message shown when the original share folder selected by the owner can't be accessed
#. Properties... text comes from the right-click menu of the application in the Unite panel.
#: templates/noSharedMountpoint.html
msgid "Folder not found. To select a new one, right-click <STRONG>{serviceName}</STRONG> in the Unite panel, and choose <STRONG>Properties</STRONG>"
msgstr "Kansiota ei löytynyt. Valitse jokin toinen kansio napsauttamalla kohdetta <STRONG>{serviceName}</STRONG> hiiren kakkospainikkeella Unite-paneelissa ja valitsemalla <STRONG>Ominaisuudet</STRONG>"

#. Text in the generated index.html file.
#: templates/index.html
msgid "This sample Web page <STRONG>index.html</STRONG> was created when you clicked \"generate a sample file\" in a folder without an index.html file. Edit it to suit your taste. This is the first page visitors to your Web Server will see."
msgstr "Tämä esimerkkisivu <STRONG>index.html</STRONG> luotiin, kun valitsit Luo esimerkkitiedosto kansiossa, jossa ei ollut index.html-tiedostoa. Voit muokata sivua makusi mukaan. Tämä on ensimmäinen sivu, jonka web-palvelimesi vierailijat näkevät."

#. A header in the generated index.html that describes a section of the page
#. for the viewer to get resources to learn Web development.
#: templates/index.html
msgid "Resources"
msgstr "Resurssit"

#. Text in the generated index.html file. Followed by a link to the Opera Web Standards Curriculum.
#: templates/index.html
msgid "To learn more about Web development and design, see the"
msgstr "Lisätietoja web-kehittelystä ja -suunnittelusta on kohteessa"

